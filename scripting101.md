# Shell Scripting Basics 
#### By: Roger Mukai
DATE: 10/26/2020
UPDATED: 10/26/2020   

[Link to info](https://bash.cyberciti.biz/guide/Getting_User_Input_Via_Keyboard)
____
# Intro
<p> Shell Scripitng can be used for many things. Personally, I have found that the more I can automate, the less frustrating 😤 my mistakes become. Specificly I started to find ways to set up my personal Development enviroment, without having to manually change settings and install my programs. why? because I make mistakes and find myself having to reinstall a clean linux image. Admitededly I am having to do that less and less often, but I figured it's a good way to introduce myself to shell scripting and I hope you might find it useful as well. 
</p>

## What I will cover
### <ul>Topics
<li>[commandline basics?](#CLI) </li><br>
<li>[Low-level Scripting](#Low-Level)</li><br>
<li>[Writing an Exacutable shell script file](#scripting)</li><br>
<li>[The Bigger Picture](#overview)</li><br>
<li>[Cheatsheet](#cheatsheet)</li><br>
</ul>

## [Comandline refference](CLI)
<p>The shell you're likely using/used to is a type of UNIX commandline Shell Interface. All Liniux is based on uinx and so is Apple's OS. I won't go into to much detail here but you can checkout the [Unix Wikipedia page](https://en.wikipedia.org/wiki/Unix_shell) to go down that rabbit hole. </p>

<p>For this introduction to the shell I will asume you're running on a Mac or Linux or [Windows Subsytem for Linux, aka WSL.(Follow Hyperlink for details)](https://docs.microsoft.com/en-us/windows/wsl/install-win10). Additonally, you should know I am using oh-my-zsh, and I assume you will as well. If you don't no worries everything should work on the basic BASH as well, it might look a little diffrent. Checkout my [DevSetup repo](https://github.com/MukaiGuy/Rogers_Development-Enviroment-Setup) or feel free to do a web search for "Oh-My-ZSH install"</p>


_____________
# [Low-Level Scripting](Low-Level)



_____________
# [Full Shell Scripts](scripting)

Shell script extentions look like this `example.sh` most of the time, but not always. <br>
<br>
The First Line of a shell script should start with the following ```#!/bin/bash``` or ```#!/bin/sh```

```bash
#!/bin/sh 

read -p "Enter your sudo password : " password
echo "Your, $password. has been entered the scripit will now use that for the sudo"

&& 

sudo apt update

```

_____________
# [Overview](overview)


_____________
# [Cheatsheet](cheatsheet)