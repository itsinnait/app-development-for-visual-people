# Welcome

[Need a Link?](links.md)

# What we are teaching eachother right now: 
We are exploring the simularities and differances between <br> 
the frameworks around __JavaScript__ & __Python__. <br>

Specifically we are looking at Farmeworks for developing web application, aka Webframeworks. <br>
If you want to review what makes Webframeworks stand out from other Farmeworks [checkout programing basics](the_fundementals/programing_101.md)<br>




Digram in Progress
[JavaScript vs Python Webframework Diagram](https://app.diagrams.net/#G1pHCEHtyRVnNRpl1nxtdiY71eXdGJmNAd)


## A few handy guides and shortcuts for anyone
<ul>
<li> [Decoding the 'Tech' Industry Jargon](no-bs_glossary.md)
<li> [Bash/Comandline](utilities/setup_terminal.md)
<li> [Shell Scripting](scripting101.md)
<li> [git](utilities/git_basics.md)
<li> [SSH](utilities/ssh_basics)
<li> [Containers/Docker](utilities/containers.md)
<li> [Using pip remotely](utilities/pip_basics.md)
<li> [Using npm](utilities/npm_basics.md)
</ul>


## Copy and Pasting is Expected
Don't feel bad if you copy and paste a lot.<br> 
We all do it, the important thing is that you learning as you do it.<br>
<ul> Here are a few Snipit Libraries 
<li> [Bash & Shell commands](shell-snips.md)
<li> [PIP and Django](py-snips.md)
<li> [npm and angular](js-snips.md)
</ul>

