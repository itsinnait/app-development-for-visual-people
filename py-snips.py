# Install PIP
export alias python='python3.8'
export alias pip='pip3'

## Step 1: Download
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
## Step 2: Install
python get-pip.py --user


# PIP upgrade 
pip install -U pip